import Vue from 'vue'
import App from './App.vue'
import VueFire from 'vuefire'
import BootstrapVue from 'bootstrap-vue'
import router from './router'
import firebase from 'firebase'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue, VueFire)

Vue.config.productionTip = false

var config = {
  apiKey: "AIzaSyBJsQ0a9PoCh9vAHrIqBZAgSil4-chGEf8",
  authDomain: "iomedia-notif.firebaseapp.com",
  databaseURL: "https://iomedia-notif.firebaseio.com",
  projectId: "iomedia-notif",
  storageBucket: "iomedia-notif.appspot.com",
  messagingSenderId: "146593890149",
  appId: "1:146593890149:web:f44f3b7552cf7c97"
};
firebase.initializeApp(config);
export var messaging = firebase.messaging();
export var database = firebase.database();

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
